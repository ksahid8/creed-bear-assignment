import React from "react";
import "./auth.css";
const AuthLayout = (props) => {
  return <div className="auth-wrapper">{props.children}</div>;
};

export default AuthLayout;
