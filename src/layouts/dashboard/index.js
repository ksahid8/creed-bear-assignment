import React from "react";
import "./dashboard.css";
import Sidebar from "./sidebar";
import Content from "./content";

const DashboardLayout = (props) => {
  return (
    <div className="dashboard-wrapper">
      <Sidebar />
      <Content>{props.children}</Content>
    </div>
  );
};

export default DashboardLayout;
