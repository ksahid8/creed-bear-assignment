import React from "react";

const Content = ({ children }) => {
  return (
    <div className="page-container">
      <div className="page-content">{children}</div>
    </div>
  );
};

export default Content;
