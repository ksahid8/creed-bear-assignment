import React from "react";
import "./dashboard.css";
import Logo from "images/logo.png";
import { sideBarPrimaryItems, sideBarSecondaryItems } from "utils/constants";
import SidebarItem from "components/sidebarItem";

const Sidebar = ({}) => {
  const loggedInUser = JSON.parse(localStorage.getItem("loggedInUser"));
  return (
    <div className="sidebar-container">
      <div className="sidebar-top">
        <div className="sidebar-logo-container">
          <img src={Logo} alt="company-logo" height={50} width={151} />
        </div>
        <div className="primary-links-container">
          {sideBarPrimaryItems?.map((item) => (
            <SidebarItem key={item.key} item={item} />
          ))}
        </div>
      </div>
      <div className="sidebar-bottom">
        <div className="secondary-links-container">
          {sideBarSecondaryItems?.map((item) => (
            <SidebarItem key={item.key} item={item} />
          ))}
        </div>
        <div className="profile-info-container">
          <div className="profile-info-image">
            <img
              alt="profile-image"
              src={loggedInUser?.avatar}
              height={30}
              width={30}
            />
          </div>
          <div className="profile-info-text">
            <span className="name">
              {loggedInUser?.firstName + " " + loggedInUser?.firstName}
            </span>
            <span className="email">{loggedInUser?.email}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
