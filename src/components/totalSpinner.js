import React, { useContext } from "react";
import { GlobalState } from "context-api";
import { Bars } from "react-loading-icons";

const TotalSpinner = () => {
  const { totalLoading } = useContext(GlobalState);
  return totalLoading ? (
    <div className="total-spinner">
      <div className="total-loading">
        <Bars fill="464ef0" />
      </div>
    </div>
  ) : null;
};

export default TotalSpinner;
