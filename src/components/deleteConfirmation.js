import React from "react";

const DeleteConfirmation = ({
  handlerDeleteUsers,
  deleteItemIds,
  setShowDeleteModal,
}) => {
  const itemsLength =
    deleteItemIds?.length === 1 ? "1 item" : deleteItemIds?.length + " items";

  return (
    <div className="delete-confirmation-card">
      Are you sure want to delete {itemsLength}
      <div className="button-container">
        <button
          className="cancel-button"
          onClick={() => setShowDeleteModal(false)}
        >
          Cancel
        </button>
        <button
          className="submit-button"
          onClick={() => handlerDeleteUsers(deleteItemIds)}
        >
          Confirm
        </button>
      </div>
    </div>
  );
};

export default DeleteConfirmation;
