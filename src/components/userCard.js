import React from "react";
import { AiOutlineEdit, AiOutlineDelete } from "react-icons/ai";
import { GrRadialSelected, GrRadial } from "react-icons/gr";
const UserCard = ({
  setShowViewModal,
  setViewItemId,
  selectedUsers,
  setSelectedUsers,
  multiSelect,
  user,
  setEditItemId,
  setShowEditModal,
  setDeleteItemIds,
  setShowDeleteModal,
}) => {
  const { id, avatar, firstName, lastName, email } = user;

  return (
    <div className="user-item" key={id}>
      {multiSelect ? (
        <div
          className={`select-circle`}
          onClick={() => {
            if (!selectedUsers.includes(id)) {
              setSelectedUsers([...selectedUsers, id]);
            } else {
              setSelectedUsers(selectedUsers.filter((item) => item !== id));
            }
          }}
        >
          {selectedUsers.includes(id) ? (
            <GrRadialSelected fill="#464ef0" />
          ) : (
            <GrRadial fill="#464ef0" />
          )}
        </div>
      ) : null}
      <div className="user-item-left">
        <div className="profile-image-container">
          <img alt="profile-image" src={avatar} height={50} width={50} />
        </div>
        <div className="profile-text-container">
          <div
            className="user-name capitalize"
            onClick={() => {
              setShowViewModal(true);
              setViewItemId(id);
            }}
          >
            {firstName + " " + lastName}
          </div>
          <div className="email">{email}</div>
        </div>
      </div>

      <div className="profile-icon-container">
        <div
          className="edit-icon"
          onClick={() => {
            setEditItemId(id);
            setShowEditModal(true);
          }}
        >
          <AiOutlineEdit />
        </div>
        <div
          className="delete-icon"
          onClick={() => {
            setDeleteItemIds([id]);
            setShowDeleteModal(true);
          }}
        >
          <AiOutlineDelete />
        </div>
      </div>
    </div>
  );
};

export default UserCard;
