import React from "react";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

const SkeletonCardsLoader = () => {
  const SkeletonCard = () => {
    return (
      <div className="skeleton-card">
        <Skeleton
          count={1}
          height={86}
          style={{ borderRadius: 10, zIndex: 0 }}
        />
      </div>
    );
  };

  return (
    <div className="skeleton-cards-loader">
      {Array.from({ length: 18 }).map((user, index) => (
        <SkeletonCard key={index} />
      ))}
    </div>
  );
};

export default SkeletonCardsLoader;
