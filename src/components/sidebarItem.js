import React, { useContext } from "react";
import { useHistory } from "react-router";
import { GlobalState } from "context-api";

const SidebarItem = ({ item }) => {
  const { setAuthentication, setTotalLoading, fetchFakeUsers } =
    useContext(GlobalState);
  const { key, icon, title, route, count } = item;
  const history = useHistory();
  return (
    <div
      key={key}
      className={`sidebar-item ${key === 6 ? "active" : ""}`}
      onClick={() => {
        if (route === "logout") {
          setTotalLoading(true);
          setTimeout(() => {
            setAuthentication({ status: false, user: null });
            localStorage.removeItem("loggedInUser");
            localStorage.removeItem("allUsers");
            history.push("/login");
            setTotalLoading(false);
            fetchFakeUsers();
          }, 2000);
        }
      }}
    >
      <div className="sidebar-item-left">
        <div className="sidebar-icon"> {icon}</div>
        <div className="sidebar-title"> {title}</div>
      </div>
      {count > 0 ? <div className="sidebar-count"> {count}</div> : null}
    </div>
  );
};

export default SidebarItem;
