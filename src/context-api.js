import React, { createContext, useEffect, useState } from "react";
import { faker } from "@faker-js/faker";

export const GlobalState = createContext();

export const GlobalStateProvider = (props) => {
  const [authentication, setAuthentication] = useState({
    status: false,
    user: null,
  });
  const [totalLoading, setTotalLoading] = useState(false);

  const fetchFakeUsers = () => {
    const USERS = [];

    function createRandomUser() {
      return {
        id: faker.datatype.uuid(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        avatar: faker.image.avatar(),
        password: faker.internet.password(),
      };
    }

    Array.from({ length: 120 }).forEach(() => {
      USERS.push(createRandomUser());
    });

    localStorage.setItem("allUsers", JSON.stringify(USERS));
  };

  useEffect(() => {
    if (!localStorage.getItem("allUsers")) {
      fetchFakeUsers();
    }
  }, [localStorage.getItem("allUsers")]);

  useEffect(() => {
    if (localStorage.getItem("loggedInUser")) {
      setAuthentication({
        status: true,
        user: JSON.parse(localStorage.getItem("loggedInUser")),
      });
    } else {
      setAuthentication({ status: false, user: null });
    }
  }, [localStorage.getItem("loggedInUser")]);

  return (
    <GlobalState.Provider
      value={{
        fetchFakeUsers,
        authentication,
        setAuthentication,
        totalLoading,
        setTotalLoading,
      }}
    >
      {props.children}
    </GlobalState.Provider>
  );
};
