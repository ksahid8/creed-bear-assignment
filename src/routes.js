import React, { useContext } from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
  useHistory,
} from "react-router-dom";
import { GlobalState } from "./context-api";
import AuthLayout from "./layouts/auth";
import DashboardLayout from "./layouts/dashboard";
import LoginPage from "./containers/login";
import UsersPage from "./containers/users";

const Routes = () => {
  const { authentication } = useContext(GlobalState);

  const history = useHistory();

  return (
    <Router histoy={history}>
      {!authentication?.status ? (
        <AuthLayout>
          <Route path="/" component={LoginPage} />
          <Redirect to="/" />
        </AuthLayout>
      ) : (
        <DashboardLayout>
          <Switch>
            <Route path="/users" component={UsersPage} />
            <Redirect to="/users" />
          </Switch>
        </DashboardLayout>
      )}
    </Router>
  );
};

export default Routes;
