import { render, screen, fire, fireEvent } from "@testing-library/react";
import LoginPage from "..";
import { GlobalStateProvider } from "context-api";

const MockLoginPage = () => {
  return (
    <GlobalStateProvider>
      <LoginPage />
    </GlobalStateProvider>
  );
};

describe("Login Page", () => {
  test("should render email input field", () => {
    render(<MockLoginPage />);
    const inputElement = screen.getByPlaceholderText(/Enter Email/i);
    expect(inputElement).toBeInTheDocument();
  });

  test("should able to type into email input field", () => {
    render(<MockLoginPage />);
    const inputElement = screen.getByPlaceholderText(/Enter Email/i);
    fireEvent.change(inputElement, { target: { value: "abc" } });
    expect(inputElement.value).toBe("abc");
  });

  test("should render password input field", () => {
    render(<MockLoginPage />);
    const inputElement = screen.getByPlaceholderText(/Enter Password/i);
    expect(inputElement).toBeInTheDocument();
  });

  test("should able to type into password input field", () => {
    render(<MockLoginPage />);
    const inputElement = screen.getByPlaceholderText(/Enter Password/i);
    fireEvent.change(inputElement, { target: { value: "abc" } });
    expect(inputElement.value).toBe("abc");
  });

  test("should render login button", () => {
    render(<MockLoginPage />);
    const buttonElement = screen.getByText(/Login/i);
    expect(buttonElement).toBeInTheDocument();
  });

  test("check whether login email is displayed", () => {
    render(<MockLoginPage />);
    const emailElement = screen.getByTestId("login-email");
    expect(emailElement.textContent).not.toBe("Email: ");
    expect(emailElement.textContent).not.toBe("Email: " + undefined);
  });

  test("check whether login password is displayed", () => {
    render(<MockLoginPage />);
    const passwordElement = screen.getByTestId("login-password");
    expect(passwordElement.textContent).not.toBe("Password: ");
    expect(passwordElement.textContent).not.toBe("Password: " + undefined);
  });
});
