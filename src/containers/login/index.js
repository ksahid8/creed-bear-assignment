import React, { useState, useContext } from "react";
import { GlobalState } from "context-api";
import Logo from "images/logo.png";
import { verifyLoginHandler } from "utils/services";
import "./style.css";

const LoginPage = () => {
  const { setTotalLoading = false } = useContext(GlobalState);

  const allUsers = JSON.parse(localStorage.getItem("allUsers"));

  const loginUser = allUsers && allUsers?.length > 0 && allUsers[0];

  const [fieldValues, setFieldValues] = useState({ email: "", password: "" });

  const [errorMessage, setErrorMessage] = useState({});

  const renderErrorMessage = (name) => {
    if (name === errorMessage.name) {
      return <div className="error">{errorMessage.message}</div>;
    }
  };

  const onChangeHandler = (name, value) => {
    setFieldValues({ ...fieldValues, [name]: value });
    setErrorMessage({});
  };

  const handleSubmit = async () => {
    const { email, password } = fieldValues;

    if (!email) {
      setErrorMessage({
        ...errorMessage,
        name: "email",
        message: "Email field is empty",
      });
      return;
    }
    if (!password) {
      setErrorMessage({
        ...errorMessage,
        name: "password",
        message: "Password field is empty",
      });
      return;
    }

    if (email && password) {
      setTotalLoading(true);
      const loginResponse = await verifyLoginHandler(email, password);
      if (loginResponse?.data?.status === "success") {
        setErrorMessage({});
        localStorage.setItem(
          "loggedInUser",
          JSON.stringify(loginResponse?.data?.user)
        );
        setTotalLoading(false);
      } else {
        setErrorMessage({
          ...errorMessage,
          name: "login",
          message: "Invalid Login Credentials",
        });
        setTotalLoading(false);
      }
    }
  };

  return (
    <div className="login-container">
      <div className="logo-container">
        <img src={Logo} alt="company-logo" height={90} width={273} />
      </div>
      <div className="form-container">
        {errorMessage?.name === "login" ? (
          <div className="login-error">{renderErrorMessage("login")}</div>
        ) : null}
        <div className="input-container">
          <label className="input-label">Email </label>
          <input
            placeholder="Enter Email"
            className="input-field"
            type="text"
            value={fieldValues?.email}
            required
            onChange={(e) => {
              onChangeHandler("email", e.target.value);
            }}
          />
          {renderErrorMessage("email")}
        </div>
        <div className="input-container">
          <label className="input-label">Password </label>
          <input
            placeholder="Enter Password"
            className="input-field"
            type="password"
            value={fieldValues?.password}
            required
            onChange={(e) => {
              onChangeHandler("password", e.target.value);
            }}
          />
          {renderErrorMessage("password")}
        </div>
        <div className="login-button" onClick={() => handleSubmit()}>
          Login
        </div>
        <div className="default-user-container">
          <span data-testid="login-email">{`Email: ${loginUser?.email}`}</span>
          <span data-testid="login-password">{`Password: ${loginUser?.password}`}</span>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
