import { render, screen, fire, fireEvent } from "@testing-library/react";
import { GlobalStateProvider } from "context-api";
import UserDetails from "../ui/details";

const MockUserDetails = () => {
  return (
    <GlobalStateProvider>
      <UserDetails />
    </GlobalStateProvider>
  );
};

describe("User Details", () => {
  test("should render detail image", () => {
    render(<MockUserDetails />);
    const buttonElement = screen.getByAltText(/detail-image/i);
    expect(buttonElement).toBeInTheDocument();
  });

  test("should render user Id", () => {
    render(<MockUserDetails />);
    const buttonElement = screen.getByTestId("userId");
    expect(buttonElement).toBeInTheDocument();
  });

  test("should render user full name", () => {
    render(<MockUserDetails />);
    const buttonElement = screen.getByTestId("userName");
    expect(buttonElement).toBeInTheDocument();
  });

  test("should render user email", () => {
    render(<MockUserDetails />);
    const buttonElement = screen.getByTestId("userEmail");
    expect(buttonElement).toBeInTheDocument();
  });
});
