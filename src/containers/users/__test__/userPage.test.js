import { render, screen, act } from "@testing-library/react";
import UsersPage from "containers/users/index";
import { GlobalStateProvider } from "context-api";

const MockUserPage = () => {
  return (
    <GlobalStateProvider>
      <UsersPage />
    </GlobalStateProvider>
  );
};

describe("User Page", () => {
  test("check page header rendered", async () => {
    await act(async () => render(<MockUserPage />));
    const pageHeaderElement = screen.getByTestId("pageHeader");
    expect(pageHeaderElement).toBeInTheDocument();
  });
  test("check pagination rendered", async () => {
    await act(async () => render(<MockUserPage />));
    const paginationlement = screen.getByTestId("pagination");
    expect(paginationlement).toBeInTheDocument();
  });
});
