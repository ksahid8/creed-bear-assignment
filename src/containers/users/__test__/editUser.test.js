import { render, screen, fire, fireEvent } from "@testing-library/react";
import { GlobalStateProvider } from "context-api";
import EditUserForm from "../ui/edit";

const MockEditUserForm = () => {
  return (
    <GlobalStateProvider>
      <EditUserForm />
    </GlobalStateProvider>
  );
};

describe("Edit User Form", () => {
  test("should render first name input field", () => {
    render(<MockEditUserForm />);
    const inputElement = screen.getByPlaceholderText(/Enter First Name/i);
    expect(inputElement).toBeInTheDocument();
  });

  test("should able to type into first name input field", () => {
    render(<MockEditUserForm />);
    const inputElement = screen.getByPlaceholderText(/Enter First Name/i);
    fireEvent.change(inputElement, { target: { value: "abc" } });
    expect(inputElement.value).toBe("abc");
  });
  test("should render last name input field", () => {
    render(<MockEditUserForm />);
    const inputElement = screen.getByPlaceholderText(/Enter Last Name/i);
    expect(inputElement).toBeInTheDocument();
  });

  test("should able to type into last name input field", () => {
    render(<MockEditUserForm />);
    const inputElement = screen.getByPlaceholderText(/Enter Last Name/i);
    fireEvent.change(inputElement, { target: { value: "abc" } });
    expect(inputElement.value).toBe("abc");
  });
  test("should render email input field", () => {
    render(<MockEditUserForm />);
    const inputElement = screen.getByPlaceholderText(/Enter Email/i);
    expect(inputElement).toBeInTheDocument();
  });

  test("should able to type into email input field", () => {
    render(<MockEditUserForm />);
    const inputElement = screen.getByPlaceholderText(/Enter Email/i);
    fireEvent.change(inputElement, { target: { value: "abc" } });
    expect(inputElement.value).toBe("abc");
  });

  test("should render update button", () => {
    render(<MockEditUserForm />);
    const buttonElement = screen.getByText(/Update/i);
    expect(buttonElement).toBeInTheDocument();
  });

  test("should render cancel button", () => {
    render(<MockEditUserForm />);
    const buttonElement = screen.getByText(/Cancel/i);
    expect(buttonElement).toBeInTheDocument();
  });
});
