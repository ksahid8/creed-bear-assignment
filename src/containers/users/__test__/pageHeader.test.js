import { render, screen } from "@testing-library/react";
import PageHeader from "containers/users/ui/pageHeader";

const mockedFunction = jest.fn();

describe("Page Header", () => {
  test("should render same text passed into heading prop", async () => {
    render(<PageHeader heading="xyz" />);
    const headingElement = screen.getByRole("heading");
    expect(headingElement.textContent).toBe("xyz");
  });
  test("should render same icon and text passed into primary button prop", async () => {
    render(<PageHeader primaryButton={["text", "icon", mockedFunction]} />);
    const buttonElement = screen.getByRole("button", { name: "icontext" });
    expect(buttonElement.textContent).toBe("icontext");
  });
  test("should render same icon and text passed into secondary button prop", async () => {
    render(<PageHeader secondaryButton={["text", "icon", mockedFunction]} />);
    const buttonElement = screen.getByRole("button", { name: "icontext" });
    expect(buttonElement.textContent).toBe("icontext");
  });
  test("check delete button is visible if selected items are not empty", async () => {
    render(
      <PageHeader
        selectedUsers={["a"]}
        deleteButton={["text", "icon", mockedFunction]}
      />
    );
    const buttonElement = screen.getByRole("button", { name: "icontext" });
    expect(buttonElement.textContent).toBe("icontext");
  });
  test("check delete button is hidden if selected items are empty", async () => {
    render(
      <PageHeader
        selectedUsers={[]}
        deleteButton={["text", "icon", mockedFunction]}
      />
    );
    const buttonElement = screen.queryByRole("button", { name: "icontext" });
    expect(buttonElement).not.toBeInTheDocument();
  });
});
