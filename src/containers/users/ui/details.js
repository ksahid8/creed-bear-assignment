import React, { useEffect, useState } from "react";
import { getUserDetailsHandler } from "utils/services";

const UserDetails = ({ viewItemId }) => {
  const [userData, setUserData] = useState({});

  const fetchUserDetails = async () => {
    let userDetails = await getUserDetailsHandler(viewItemId);

    if (userDetails) {
      const { data } = userDetails;
      setUserData(data);
    }
  };

  useEffect(() => {
    if (viewItemId) {
      fetchUserDetails();
    }
  }, [viewItemId]);
  return (
    <div className="user-detail-card">
      <div className="image-container">
        <img
          alt="detail-image"
          src={userData?.avatar}
          height={100}
          width={100}
        />
      </div>
      <div className="text-container">
        <div className="detail-user-id" data-testid="userId">
          {userData?.id}
        </div>
        <div className="detail-user-name" data-testid="userName">
          {userData?.firstName + " " + userData?.lastName}
        </div>
        <div className="detail-user-email" data-testid="userEmail">
          {userData?.email}
        </div>
      </div>
    </div>
  );
};

export default UserDetails;
