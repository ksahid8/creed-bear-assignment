import React, { useState, useContext } from "react";
import { GlobalState } from "context-api";
import { addUserHandler } from "utils/services";
import { faker } from "@faker-js/faker";
import { Store } from "react-notifications-component";
import { alertOptions } from "utils/constants";

const AddUserForm = ({ setShowAddModal, fetchUsers }) => {
  const { setTotalLoading } = useContext(GlobalState);
  const [showErrors, setShowErrors] = useState(false);
  const [fieldValues, setFieldValues] = useState({
    email: { value: "", isValid: false },
    firstName: { value: "", isValid: false },
    lastName: { value: "", isValid: false },
  });

  const onChangeHandler = (name, value) => {
    setFieldValues({
      ...fieldValues,
      [name]: { value: value, isValid: value?.length > 0 ? true : false },
    });
  };

  const renderErrorMessage = (name) => {
    let errorMessage;

    if (name === "firstName" && showErrors) {
      if (!fieldValues?.firstName?.isValid) {
        errorMessage = "First Name Field is Empty";
      }
    } else if (name === "lastName" && showErrors) {
      if (!fieldValues?.lastName?.isValid) {
        errorMessage = "Last Name Field is Empty";
      }
    } else if (name === "email") {
      if (!fieldValues?.email?.isValid && showErrors) {
        errorMessage = "Email Field is Empty";
      }
    }
    return <div className="error">{errorMessage}</div>;
  };

  const submitHandler = async () => {
    const { email, firstName, lastName } = fieldValues;

    if (email?.isValid && firstName?.isValid && lastName?.isValid) {
      setTotalLoading(true);
      let payload = {
        id: faker.datatype.uuid(),
        avatar: faker.image.avatar(),
        password: faker.internet.password(),
        firstName: fieldValues?.firstName?.value,
        lastName: fieldValues?.lastName?.value,
        email: fieldValues?.email?.value,
      };

      const addResponse = await addUserHandler(payload);
      if (addResponse?.data?.status === "success") {
        Store.addNotification({
          title: "Success!",
          message: "User Created Successfully",
          type: "success",
          ...alertOptions,
        });
        fetchUsers(1);
        setTotalLoading(false);
        setShowAddModal(false);
      }
    } else {
      setShowErrors(true);
    }
  };

  return (
    <div className="form-container">
      <div className="input-container">
        <label className="input-label">First Name</label>
        <input
          className="input-field"
          value={fieldValues?.firstName?.value}
          onChange={(e) => {
            onChangeHandler("firstName", e.target.value);
          }}
          placeholder="Enter First Name"
        />
        {renderErrorMessage("firstName")}
      </div>
      <div className="input-container">
        <label className="input-label">Last Name</label>
        <input
          className="input-field"
          value={fieldValues?.lastName?.value}
          onChange={(e) => {
            onChangeHandler("lastName", e.target.value);
          }}
          placeholder="Enter Last Name"
        />
        {renderErrorMessage("lastName")}
      </div>
      <div className="input-container">
        <label className="input-label">Email</label>
        <input
          className="input-field"
          value={fieldValues?.email?.value}
          onChange={(e) => {
            onChangeHandler("email", e.target.value);
          }}
          placeholder="Enter Email"
        />
        {renderErrorMessage("email")}
      </div>
      <div className="button-container">
        <button
          className="cancel-button"
          onClick={() => setShowAddModal(false)}
        >
          Cancel
        </button>
        <button className="submit-button" onClick={() => submitHandler()}>
          Submit
        </button>
      </div>
    </div>
  );
};

export default AddUserForm;
