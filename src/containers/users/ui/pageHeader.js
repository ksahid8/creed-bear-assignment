import React from "react";

const PageHeader = ({
  selectedUsers = [],
  heading,
  primaryButton = [],
  secondaryButton = [],
  deleteButton = [],
}) => {
  return (
    <div className="page-header-container" data-testid="pageHeader">
      <h3 className="page-heading">{heading}</h3>
      <div className="heading-buttons-container">
        {selectedUsers?.length > 0 ? (
          <button onClick={deleteButton[2]} className="danger">
            {deleteButton[1]}
            {deleteButton[0]}
          </button>
        ) : null}
        <button onClick={secondaryButton[2]} className="secondary">
          {secondaryButton[1]}
          {secondaryButton[0]}
        </button>
        <button
          onClick={primaryButton[2]}
          className="primary"
          data-testid="primary-button"
        >
          {primaryButton[1]}
          {primaryButton[0]}
        </button>
      </div>
    </div>
  );
};

export default PageHeader;
