import React, { useState, useContext, useEffect } from "react";
import { GlobalState } from "context-api";
import { updateUserHandler, getUserDetailsHandler } from "utils/services";
import { Store } from "react-notifications-component";
import { alertOptions } from "utils/constants";

const EditUserForm = ({ setShowEditModal, editItemId, fetchUsers }) => {
  const { setTotalLoading } = useContext(GlobalState);
  const [showErrors, setShowErrors] = useState(false);
  const [fieldValues, setFieldValues] = useState({
    email: { value: "", isValid: false },
    firstName: { value: "", isValid: false },
    lastName: { value: "", isValid: false },
  });

  const onChangeHandler = (name, value) => {
    setFieldValues({
      ...fieldValues,
      [name]: { value: value, isValid: value?.length > 0 ? true : false },
    });
  };

  const renderErrorMessage = (name) => {
    let errorMessage;

    if (name === "firstName" && showErrors) {
      if (!fieldValues?.firstName?.isValid) {
        errorMessage = "First Name Field is Empty";
      }
    } else if (name === "lastName" && showErrors) {
      if (!fieldValues?.lastName?.isValid) {
        errorMessage = "Last Name Field is Empty";
      }
    } else if (name === "email") {
      if (!fieldValues?.email?.isValid && showErrors) {
        errorMessage = "Email Field is Empty";
      }
    }
    return <div className="error">{errorMessage}</div>;
  };

  const submitHandler = async () => {
    const { email, firstName, lastName } = fieldValues;
    if (email?.isValid && firstName?.isValid && lastName?.isValid) {
      setTotalLoading(true);
      let payload = {
        id: editItemId,
        firstName: fieldValues?.firstName?.value,
        lastName: fieldValues?.lastName?.value,
        email: fieldValues?.email?.value,
      };
      const editResponse = await updateUserHandler(payload);
      if (editResponse?.data?.status === "success") {
        Store.addNotification({
          title: "Success!",
          message: "User Updated Successfully",
          type: "success",
          ...alertOptions,
        });
        fetchUsers(1);
        setTotalLoading(false);
        setShowEditModal(false);
      }
    } else {
      setShowErrors(true);
    }
  };

  const fetchUserDetails = async () => {
    let userDetails = await getUserDetailsHandler(editItemId);

    if (userDetails) {
      const { data } = userDetails;

      setFieldValues({
        email: { value: data?.email, isValid: true },
        firstName: { value: data?.firstName, isValid: true },
        lastName: { value: data?.lastName, isValid: true },
      });
    }
  };

  useEffect(() => {
    if (editItemId) {
      fetchUserDetails();
    }
  }, [editItemId]);

  return (
    <div className="form-container">
      <div className="input-container">
        <label className="input-label">First Name</label>
        <input
          className="input-field"
          value={fieldValues?.firstName?.value}
          onChange={(e) => {
            onChangeHandler("firstName", e.target.value);
          }}
          placeholder="Enter First Name"
        />
        {renderErrorMessage("firstName")}
      </div>
      <div className="input-container">
        <label className="input-label">Last Name</label>
        <input
          className="input-field"
          value={fieldValues?.lastName?.value}
          onChange={(e) => {
            onChangeHandler("lastName", e.target.value);
          }}
          placeholder="Enter Last Name"
        />
        {renderErrorMessage("lastName")}
      </div>
      <div className="input-container">
        <label className="input-label">Email</label>
        <input
          className="input-field"
          value={fieldValues?.email?.value}
          onChange={(e) => {
            onChangeHandler("email", e.target.value);
          }}
          placeholder="Enter Email"
        />
        {renderErrorMessage("email")}
      </div>
      <div className="button-container">
        <button
          className="cancel-button"
          onClick={() => setShowEditModal(false)}
        >
          Cancel
        </button>
        <button className="submit-button" onClick={() => submitHandler()}>
          Update
        </button>
      </div>
    </div>
  );
};

export default EditUserForm;
