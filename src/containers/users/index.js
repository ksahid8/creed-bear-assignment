import React, { useEffect, useState, useContext } from "react";
import { getUsersHandler, deleteUsersHandler } from "utils/services";
import "./style.css";
import ReactPaginate from "react-paginate";
import SkeletonCardsLoader from "components/skeletonCardsLoader";
import UserCard from "components/userCard";
import { AiOutlinePlus } from "react-icons/ai";
import PageHeader from "./ui/pageHeader";
import CustomModal from "components/customModal";
import AddUserForm from "./ui/add";
import EditUserForm from "./ui/edit";
import { Store } from "react-notifications-component";
import { alertOptions } from "utils/constants";
import { GlobalState } from "context-api";
import { RiCheckboxMultipleFill } from "react-icons/ri";
import { AiOutlineDelete } from "react-icons/ai";
import UserDetails from "./ui/details";
import DeleteConfirmation from "components/deleteConfirmation";

const UsersPage = () => {
  const { setTotalLoading } = useContext(GlobalState);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [showAddModal, setShowAddModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showViewModal, setShowViewModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [editItemId, setEditItemId] = useState("");
  const [multiSelect, setMultiSelect] = useState(false);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [viewItemId, setViewItemId] = useState("");
  const [deleteItemIds, setDeleteItemIds] = useState([]);

  const [pagination, setPagination] = useState({
    page: 1,
    perPage: 18,
    total: null,
    totalPages: 2,
  });

  const fetchUsers = async (page) => {
    setLoading(true);
    let payload = {
      page: page ? page : pagination?.page,
      perPage: pagination?.perPage,
    };
    const usersResponse = await getUsersHandler(payload);
    if (usersResponse) {
      setPagination({
        page: usersResponse?.page,
        perPage: usersResponse?.perPage,
        total: usersResponse?.total,
        totalPages: usersResponse?.totalPages,
      });
    }
    setData(usersResponse?.data);
    setLoading(false);
  };

  const handlePageClick = (event) => {
    const { selected } = event;
    fetchUsers(selected + 1);
  };

  const handlerDeleteUsers = async (ids) => {
    setTotalLoading(true);
    let deleteResponse = await deleteUsersHandler(ids);
    if (deleteResponse?.data?.status === "success") {
      Store.addNotification({
        title: "Success!",
        message: "Users Deleted Successfully",
        type: "success",
        ...alertOptions,
      });
      setShowDeleteModal(false);
      setDeleteItemIds([]);
      fetchUsers(1);
      setTotalLoading(false);
      setSelectedUsers([]);
      setMultiSelect(false);
    }
  };
  useEffect(() => {
    fetchUsers();
  }, []);

  return (
    <>
      <CustomModal
        modalHeading="User Details"
        isOpen={showViewModal}
        closeModal={() => setShowViewModal(false)}
      >
        <UserDetails viewItemId={viewItemId} />
      </CustomModal>
      <CustomModal
        modalHeading="Add User"
        isOpen={showAddModal}
        closeModal={() => setShowAddModal(false)}
      >
        <AddUserForm
          setShowAddModal={setShowAddModal}
          fetchUsers={fetchUsers}
        />
      </CustomModal>
      <CustomModal
        modalHeading="Edit User"
        isOpen={showEditModal}
        closeModal={() => setShowEditModal(false)}
      >
        <EditUserForm
          editItemId={editItemId}
          setShowEditModal={setShowEditModal}
          fetchUsers={fetchUsers}
        />
      </CustomModal>
      <CustomModal
        modalHeading="Delete Confirmation"
        isOpen={showDeleteModal}
        closeModal={() => setShowDeleteModal(false)}
      >
        <DeleteConfirmation
          setShowDeleteModal={setShowDeleteModal}
          deleteItemIds={deleteItemIds}
          handlerDeleteUsers={handlerDeleteUsers}
        />
      </CustomModal>
      <PageHeader
        selectedUsers={selectedUsers}
        primaryButton={[
          "Add User",
          <AiOutlinePlus />,
          () => setShowAddModal(true),
        ]}
        secondaryButton={[
          "Select",
          <RiCheckboxMultipleFill />,
          () => {
            if (multiSelect) {
              setSelectedUsers([]);
              setMultiSelect(false);
            } else {
              setMultiSelect(true);
            }
          },
        ]}
        deleteButton={[
          "Delete Selected",
          <AiOutlineDelete />,
          () => {
            setDeleteItemIds(selectedUsers);
            setShowDeleteModal(true);
          },
        ]}
        heading={"Users"}
      />
      {loading ? (
        <SkeletonCardsLoader />
      ) : (
        <>
          <div className="users-container">
            {data?.map((user) => (
              <UserCard
                setShowViewModal={setShowViewModal}
                setViewItemId={setViewItemId}
                selectedUsers={selectedUsers}
                setSelectedUsers={setSelectedUsers}
                multiSelect={multiSelect}
                user={user}
                key={user?.id}
                setEditItemId={setEditItemId}
                setShowEditModal={setShowEditModal}
                setDeleteItemIds={setDeleteItemIds}
                setShowDeleteModal={setShowDeleteModal}
              />
            ))}
          </div>
        </>
      )}
      <div className="pagination" data-testid="pagination">
        <ReactPaginate
          containerClassName="pagination-container"
          breakLabel="..."
          nextLabel="next"
          onPageChange={handlePageClick}
          forcePage={pagination?.page - 1}
          pageCount={Math.ceil(pagination?.totalPages)}
          previousLabel="previous"
          renderOnZeroPageCount={null}
        />
      </div>
    </>
  );
};

export default UsersPage;
