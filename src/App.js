import React from "react";
import { GlobalStateProvider } from "./context-api";
import Routes from "./routes";
import TotalSpinner from "./components/totalSpinner";
import { ReactNotifications } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";

const App = () => {
  return (
    <GlobalStateProvider>
      <ReactNotifications />
      <TotalSpinner />
      <Routes />
    </GlobalStateProvider>
  );
};

export default App;
