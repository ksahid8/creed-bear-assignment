import {
  verifyLoginApi,
  getUsersApi,
  addUserApi,
  getUserDetailsApi,
  updateUserApi,
  deleteUsersApi,
} from "./apis";

export const verifyLoginHandler = async (email, password) => {
  try {
    const result = await verifyLoginApi(email, password);
    return result;
  } catch (error) {
    console.log(error);
  }
};

export const getUsersHandler = async (payload) => {
  try {
    const result = await getUsersApi(payload);
    return result;
  } catch (error) {
    console.log(error);
  }
};

export const addUserHandler = async (payload) => {
  try {
    const result = await addUserApi(payload);
    return result;
  } catch (error) {
    console.log(error);
  }
};

export const getUserDetailsHandler = async (id) => {
  try {
    const result = await getUserDetailsApi(id);
    return result;
  } catch (error) {
    console.log(error);
  }
};

export const updateUserHandler = async (payload) => {
  try {
    const result = await updateUserApi(payload);
    return result;
  } catch (error) {
    console.log(error);
  }
};
export const deleteUsersHandler = async (payload) => {
  try {
    const result = await deleteUsersApi(payload);
    return result;
  } catch (error) {
    console.log(error);
  }
};
