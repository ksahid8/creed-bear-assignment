import { AiOutlineDashboard, AiOutlineSetting } from "react-icons/ai";
import { RiHome6Line } from "react-icons/ri";
import { FaTasks, FaRegUserCircle } from "react-icons/fa";
import { FiPieChart, FiLogOut } from "react-icons/fi";
import { IoIosApps } from "react-icons/io";
import {
  MdOutlineContactSupport,
  MdOutlineNotificationsNone,
} from "react-icons/md";

export const alertOptions = {
  insert: "top",
  container: "bottom-left",
  animationIn: ["zoom-in"],
  animationOut: ["zoom-out"],
  dismiss: {
    duration: 3000,
  },
};

export const sideBarPrimaryItems = [
  {
    key: 1,
    title: "Home",
    route: "home",
    icon: <RiHome6Line />,
    count: 0,
  },
  {
    key: 2,
    title: "Dashboard",
    route: "dashboard",
    icon: <AiOutlineDashboard />,
    count: 0,
  },
  {
    key: 3,
    title: "Projects",
    route: "projects",
    icon: <IoIosApps />,
    count: 0,
  },
  {
    key: 4,
    title: "Tasks",
    route: "tasks",
    icon: <FaTasks />,
    count: 10,
  },
  {
    key: 5,
    title: "Reporting",
    route: "reporting",
    icon: <FiPieChart />,
    count: 0,
  },
  {
    key: 6,
    title: "Users",
    route: "users",
    icon: <FaRegUserCircle />,
    count: 0,
  },
];

export const sideBarSecondaryItems = [
  {
    key: 1,
    title: "Notifications",
    route: "notifications",
    icon: <MdOutlineNotificationsNone />,
    count: 4,
  },
  {
    key: 2,
    title: "Support",
    route: "support",
    icon: <MdOutlineContactSupport />,
    count: 0,
  },
  {
    key: 3,
    title: "Settings",
    route: "settings",
    icon: <AiOutlineSetting />,
    count: 0,
  },
  {
    key: 4,
    title: "Logout",
    route: "logout",
    icon: <FiLogOut />,
    count: 0,
  },
];
