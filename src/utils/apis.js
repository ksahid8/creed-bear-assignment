export const verifyLoginApi = (email, password) =>
  new Promise((resolve, reject) => {
    if (!email || !password) {
      reject(new Error("credentials not provided"));
    }
    const allUsers = JSON.parse(localStorage.getItem("allUsers"));
    const existUser = allUsers.filter(
      (user) => user?.email === email && user?.password === password
    );
    if (existUser?.length > 0) {
      setTimeout(
        () =>
          resolve({
            data: {
              status: "success",
              user: {
                id: existUser[0]?.id,
                email: existUser[0]?.email,
                firstName: existUser[0]?.lastName,
                lastName: existUser[0]?.firstName,
                avatar: existUser[0]?.avatar,
              },
            },
          }),
        2000
      );
    } else {
      setTimeout(
        () =>
          resolve({
            data: {
              status: "error",
            },
          }),
        1100
      );
    }
  });

export const getUsersApi = (payload) =>
  new Promise((resolve, reject) => {
    const { page, perPage } = payload;

    if (!payload) {
      reject(new Error("payload not provided"));
    }
    const allUsers = JSON.parse(localStorage.getItem("allUsers")).reverse();

    const totalCount = allUsers?.length;

    const paginatedData = allUsers.slice((page - 1) * perPage, page * perPage);

    let response = {
      page: page,
      perPage: perPage,
      total: totalCount,
      totalPages: totalCount / perPage,
      data: paginatedData,
    };

    setTimeout(() => resolve(response), 1100);
  });

export const addUserApi = (payload) =>
  new Promise((resolve, reject) => {
    (async () => {
      if (!payload) {
        reject(new Error("payload not provided"));
      }
      const allUsers = await JSON.parse(localStorage.getItem("allUsers"));

      await allUsers.push(payload);

      localStorage.setItem("allUsers", JSON.stringify(allUsers));

      setTimeout(
        () =>
          resolve({
            data: {
              status: "success",
            },
          }),
        500
      );
    })();
  });

export const getUserDetailsApi = (id) =>
  new Promise((resolve, reject) => {
    if (!id) {
      reject(new Error("user id not provided"));
    }
    const allUsers = JSON.parse(localStorage.getItem("allUsers"));

    const userDetail = allUsers.filter((user) => user?.id === id)[0];

    let response = {
      data: {
        id: userDetail?.id,
        email: userDetail?.email,
        firstName: userDetail?.firstName,
        lastName: userDetail?.lastName,
        avatar: userDetail?.avatar,
      },
    };

    resolve(response);
  });

export const updateUserApi = (payload) =>
  new Promise((resolve, reject) => {
    (async () => {
      if (!payload) {
        reject(new Error("payload not provided"));
      }
      const { id, firstName, lastName, email } = payload;
      const allUsers = await JSON.parse(localStorage.getItem("allUsers"));

      const updatedUsers = allUsers.map((user) => {
        if (user?.id === id) {
          return {
            ...user,
            firstName: firstName,
            lastName: lastName,
            email: email,
          };
        } else {
          return user;
        }
      });

      localStorage.setItem("allUsers", JSON.stringify(updatedUsers));

      setTimeout(
        () =>
          resolve({
            data: {
              status: "success",
            },
          }),
        500
      );
    })();
  });

export const deleteUsersApi = (payload) =>
  new Promise((resolve, reject) => {
    (async () => {
      if (!payload) {
        reject(new Error("payload not provided"));
      }
      const allUsers = await JSON.parse(localStorage.getItem("allUsers"));
      const updatedUsers = allUsers
        .filter((item) => !payload.includes(item?.id))
        .map((item) => item);

      localStorage.setItem("allUsers", JSON.stringify(updatedUsers));

      setTimeout(
        () =>
          resolve({
            data: {
              status: "success",
            },
          }),
        500
      );
    })();
  });
